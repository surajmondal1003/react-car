import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 47e2676e1a534f1b9c9deedf20e380647333c6aaffb95e684087f2130529c4d5'
    }
});