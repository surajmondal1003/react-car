import React from 'react';
import axios from 'axios';
import unsplash from '../api/unsplash';
import Searchbar from './Searchbar';
import ImageList from './ImageList';
import Spinner from './Spinner';

class App extends React.Component {
    state = { images: [], spinner: false };

    onSearchSubmit = async (term) => {
        this.setState({ spinner: true });

        // console.log(term);

        //promise based syntax
        // axios.get('https://api.unsplash.com/search/photos',{
        //     params:{ query:term },
        //     headers:{
        //         Authorization: 'Client-ID 47e2676e1a534f1b9c9deedf20e380647333c6aaffb95e684087f2130529c4d5'
        //     }
        // }).then((response)=>{
        //     console.log(response.data.results);// promise

        // });

        //async await syntax
        const response = await unsplash.get('search/photos', {
            params: { query: term },

        });

        this.setState({ images: response.data.results , spinner:false});
    }

    loadSpinner() {
        return this.state.spinner ? <Spinner /> : '';
    }
    render() {

        if (!this.state.spinner) {
            return (
                <div className="ui container" style={{ marginTop: '10px' }}>
                    <Searchbar onSubmit={this.onSearchSubmit} />
                    Found {this.state.images.length} images
                <ImageList images={this.state.images} />
                </div>
            );
        }
        else {
            return (
                <div className="ui container" style={{ marginTop: '10px' }}>
                    <Searchbar onSubmit={this.onSearchSubmit} />
                    <Spinner />
                </div>
            );
        }
    }
}

export default App;